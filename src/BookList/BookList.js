import './BookList.css';
import "slick-carousel/slick/slick.css";
import Slider from "react-slick";
const BookList = props => {
    const settings = {
        dots: true,
        arrows: true,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000
    }
    return(
      <div className="book-list">
        
        <h3>{props.title}</h3>
        <div className="book-gp">
                
            <Slider {...settings}>
                {props.books.map(b => {
                return (
                    <div className="book-wrapper">
                        <diiv className="book">
                            <img src={b.book_image} />

                            <div className="book-info">
                                <a href={b.amazon_product_url} target="_blank">
                                        <h4>{b.title}</h4>
                                </a>
                                <h5><i>By</i> {b.author}</h5>
                                <p>{b.description}</p>
                            </div>

                        </diiv>

                    </div>
                )})}

            </Slider>
        </div>
      </div>
  
    )
};

export default BookList;