import React, {useState, useEffect} from "react";
import BookList from "./../BookList/BookList";
import './BestSeller.css';
import ListGroup from 'react-bootstrap/ListGroup';
import Row from 'react-bootstrap/Row';
import Tab from 'react-bootstrap/Tab';
import Col from 'react-bootstrap/Col';

const BestSeller = prop => {
  const [bookLists, setBookLists] = useState([]);
  const [firstItem, setFirstItem] = useState();
 
   useEffect(() => {
     
    fetch("https://api.nytimes.com/svc/books/v3/lists/overview.json?api-key=UB3eurFGpqb75f6ic1GnGoSieFGLUbcJ")
      .then(res => res.json())
      .then(json => {
        console.log(json.results);
        setBookLists(json.results.lists);
        setFirstItem("#"+json.results.lists[0].list_name_encoded);
      });
   }, []);

   return (
     <section className="best-seller">
      <div className="container-fluid">
        <h1 className="text-center">Best Seller Books of The Week</h1>
        <div className="row justify-content-start">
          <div className="col-md-10">
            <div className="book-list">
                  <div className="book-list">
                    <Tab.Container id="list-group-tabs-example" defaultActiveKey={firstItem}>
                      <Row>
                        <Col sm={3}>
                          <ListGroup>

                            {bookLists.map(b => {
                              return(
                              <ListGroup.Item action href={"#"+b.list_name_encoded}>
                                {b.display_name}
                              </ListGroup.Item>

                            )})}
                            
                          </ListGroup>
                        </Col>
                        <Col sm={9}>
                          <Tab.Content>
                          {bookLists.map(bL => {
                            return(
                              <Tab.Pane eventKey={"#"+bL.list_name_encoded}>
                                <BookList books={bL.books} title={bL.display_name}></BookList>
                              </Tab.Pane>

                            )})}
                           
                          </Tab.Content>
                        </Col>
                      </Row>
                    </Tab.Container>
          
                  </div>

             
            </div>
            
            
          </div>
        </div>
        

      </div>
      
     </section>
   );
}

export default BestSeller;